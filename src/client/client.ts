import { RequestContext } from "@toincrease/node-context"
import * as request from "../request/request"
import * as endpoints from "../endpoints";
import * as url from "url";

export abstract class BaseClient {
    constructor(public serviceName: string, private serviceEndpoint: string) { }

    // returns the full url for a given path
    private endpoint(path: string): Promise<url.Url> {
        return endpoints.normalizeEndpoint(this.serviceEndpoint, this.serviceName).then(x => url.parse(x + path));
    }

    protected newRequest(c: RequestContext, method: string, path: string, ...statusCode: number[]): request.IRequest {
        return request.newRequest(c, method, this.endpoint(path), ...statusCode)
    }
}
