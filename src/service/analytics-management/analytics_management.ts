import {RequestContext} from "@toincrease/node-context";
import {Observable} from "@reactivex/rxjs";
import {BaseClient} from "../../client/client"
const serviceName = "analytics-management-service";


export namespace analytics_management {
    export interface PostIdentificationInput {
        machine_trn: string
        system_of_reference: string
        external_id: string
    }

    export interface DeleteIdentificationInput {
        external_id: string
        system_of_reference: string
    }

    export interface DeleteAllIdentificationsInput {
        machine_trn: string
    }

    export interface AnalyticsManagementApi {
        postIdentification(c: RequestContext, input: PostIdentificationInput): Observable<Object>
        deleteIdentification(c: RequestContext, input: DeleteIdentificationInput): Observable<Object>
        deleteAllIdentifications(c: RequestContext, input: DeleteAllIdentificationsInput): Observable<Object>
    }

    export class AnalyticsManagementClient extends BaseClient implements AnalyticsManagementApi {
        constructor(serviceEndpoint: string) {
            super(serviceName, serviceEndpoint)
        }

        postIdentification(c: RequestContext, input: PostIdentificationInput): Observable<Object> {
            return this.newRequest(c, "post", "/" + input.machine_trn + "/identification", 200)
            .withPayload(input).send();
        }

        deleteIdentification(c: RequestContext, input: DeleteIdentificationInput): Observable<Object> {
            return this.newRequest(c, "delete",
             "/" + input.system_of_reference + "/" + input.external_id, 200).send();
        }
        deleteAllIdentifications(c: RequestContext, input: DeleteAllIdentificationsInput): Observable<Object> {
            return this.newRequest(c, "delete", "/" + input.machine_trn + "/identification", 200).send();
        }
    }
}
