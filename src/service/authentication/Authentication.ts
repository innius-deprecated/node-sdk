import { RequestContext } from "@toincrease/node-context";
import { Observable } from "@reactivex/rxjs";

import * as httpstatus from "http-status-codes"
import { BaseClient } from "../../client/client";

const serviceName = "authentication-service";

export const validateTokenPath = "/verifytoken";
export const createServiceTokenPath = "/svctoken";


export namespace authentication {
    export class ValidateTokenInput {
        token: string
    }

    export interface ValidateTokenOutput {
        aud?: string,
        exp?: number,
        iat?: number,
        iss?: string,
        sub?: string,
        scp?: string[],
        user_metadata?: {
            companyid: string,
            userid?: string,
            domain?: string,
        }
        statusCode: number
        statusMessage: string
    }

    export interface CreateServiceTokenInput {
        audience: string
        company: string
        scopes: string[]
    }

    export interface AuthenticationApi {
        validateToken(c: RequestContext, input: ValidateTokenInput): Observable<ValidateTokenOutput>
        createServiceToken(c: RequestContext, input: CreateServiceTokenInput): Observable<string>
    }

    export class AuthenticationClient extends BaseClient implements AuthenticationApi {

        constructor(serviceEndpoint: string) {
            super(serviceName, serviceEndpoint)
        }

        validateToken(c: RequestContext, input: ValidateTokenInput): Observable<ValidateTokenOutput> {
            return this.newRequest(c, "post", validateTokenPath, httpstatus.OK, httpstatus.FORBIDDEN).withPayload(input).send()
                .map((x) => {
                    let res: ValidateTokenOutput = {
                        statusCode: x.statusCode(),
                        statusMessage: x.statusMessage(),
                    }

                    res = Object.assign(res, x.json());

                    return res;
                });
        }

        createServiceToken(c: RequestContext, input: CreateServiceTokenInput): Observable<string> {
            return this.newRequest(c, "post", createServiceTokenPath, httpstatus.OK).withPayload(input).send()
                .map(x => x.raw_body());
        }
    }
}