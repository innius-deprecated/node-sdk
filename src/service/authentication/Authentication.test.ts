import { assert } from "chai"
import * as mb from "@toincrease/node-mountebank";
import { newRequestContext } from "@toincrease/node-context";
import { authentication, validateTokenPath, createServiceTokenPath } from "./Authentication"

describe("Authentication", () => {
    describe("#validateToken", () => {
        var response: any = {
            "aud": "tGvsCT4SRnIAMcpUvhFHudZZDAor42fz",
            "exp": 1459279958,
            "iat": 1459243958,
            "iss": "https://to-increase.auth0.com/",
            "sub": "auth0|56e811294a315645777e5072",
            "user_metadata": {
                "companyid": "plantage",
                "userid": "ronvdw"
            },
            "scp": ["view", "edit"]
        }

        var imposter = new mb.DefaultImposter(validateTokenPath, mb.HttpMethod.POST, response, 200)

        before((done) => {
            imposter.create((err: Error, im: mb.Imposter) => {
                imposter = im;
                done(err)
            });
        })

        var client: authentication.AuthenticationClient;
        var req: authentication.ValidateTokenInput = {
            token: "bearer blablabla"
        }
        var result: authentication.ValidateTokenOutput;
        it("invocation should return", (done) => {
            client = new authentication.AuthenticationClient("http://localhost:" + imposter.port)

            client.validateToken(newRequestContext(), req).subscribe(
                x => result = x, done, done
            )
        })

        it("should return the companyid", () => {
            assert.equal(result.user_metadata.companyid, "plantage");
        })
        it("should return the userid", () => {
            assert.equal(result.user_metadata.userid, "ronvdw");
        })
        it("should return the scopes", () => {
            assert.deepEqual(result.scp, ["view", "edit"]);
        })
        it("should return the issuer", () => {
            assert.equal(result.iss, "https://to-increase.auth0.com/");
        })
        it("should have the audience", () => {
            assert.equal(result.aud, "tGvsCT4SRnIAMcpUvhFHudZZDAor42fz");
        })
        it("should have the subject", () => {
            assert.equal(result.sub, "auth0|56e811294a315645777e5072");
        })
        it("should have the expiration date", () => {
            assert.equal(result.exp, 1459279958);
        })
        describe("#unauthorized", () => {
            let imposter = new mb.DefaultImposter(validateTokenPath, mb.HttpMethod.POST, response, 403)

            let client: authentication.AuthenticationClient;
            let req: authentication.ValidateTokenInput = {
                token: "bearer blablabla"
            }
            let result: authentication.ValidateTokenOutput;

            before((done) => {
                imposter.create((err: Error, im: mb.Imposter) => {
                    imposter = im;
                    done(err)
                });
            })

            it("invocation should return", (done) => {
                client = new authentication.AuthenticationClient("http://localhost:" + imposter.port)

                client.validateToken(newRequestContext(), req).subscribe(
                    x => result = x, done, done
                )
            })
            it("status code should be 403", () => {
                assert.equal(result.statusCode, 403)
            })

        })
    });

    describe("#createServiceToken", () => {
        let r = {
            audience: "live audience",
            company: "plantage",
            scopes: ["a", "b"],
        };

        let token = "super-secret-token";

        let pred = new mb.EqualPredicate().withMethod(mb.HttpMethod.POST).withPath(createServiceTokenPath)
        let resp = new mb.Response().withStatusCode(200).withHeader("Content-Type", "text/plain; charset=utf-8").withBOdy(token)

        let imposter = new mb.Imposter()
            .withStub(new mb.Stub().withPredicate(pred).withResponse(resp))
            .withStub(new mb.Stub().withResponse(new mb.NotFoundResponse()));

        before((done) => {
            imposter.create((err: Error, im: mb.Imposter) => {
                imposter = im;
                done(err)
            });
        })

        let result: string;
        it("invoke the service client", (done) => {
            let client = new authentication.AuthenticationClient("http://localhost:" + imposter.port)
            client.createServiceToken(newRequestContext(), r).subscribe(x => result = x, done, done);
        })

        it("should return the token as a string", () => {
            assert.equal(result, token);
        })
    })
});
