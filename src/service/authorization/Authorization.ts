import { TRN } from "@toincrease/node-trn"
import { RequestContext } from "@toincrease/node-context";
import { Observable } from "@reactivex/rxjs";

import { BaseClient } from "../../client/client"

const serviceName = "authorization-service";
export const authorizeCommandPath = "/authorize";

export namespace authorization {
    export interface AuthorizationApi {
        authorize(c: RequestContext, input: AuthorizationInput): Observable<AuthorizationOutput>
    }

    export enum Effect {
        deny = 0,
        allow
    }

    export class AuthorizationInput {
        constructor(public subject: TRN, public resource: TRN, public action: string) { }

        toJSON(): any {
            return {
                subject: this.subject.toString(),
                resource: this.resource.toString(),
                action: this.action,
            }
        }
    }


    export class AuthorizationOutput {
        constructor(public subject: TRN, public resource: TRN, public action: string, public effect: Effect) { }
    }

    export class AuthorizationClient extends BaseClient implements AuthorizationApi {
        constructor(serviceEndpoint: string) {
            super(serviceName, serviceEndpoint)
        }

        authorize(c: RequestContext, input: AuthorizationInput): Observable<AuthorizationOutput> {
            return this.newRequest(c, "post", authorizeCommandPath, 200).withPayload(input).send()
                .map(x => x.json())
                .map((x: any): AuthorizationOutput => {
                    return new AuthorizationOutput(TRN.parse(x.subject), TRN.parse(x.resource), x.action, <Effect>x.effect)
                });
        }
    }
}
