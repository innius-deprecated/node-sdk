import {assert} from "chai"
import {authorization, authorizeCommandPath, } from "./Authorization"
import {newRequestContext} from "@toincrease/node-context";
import {TRN, ResourceType} from "@toincrease/node-trn"
import * as mb from "@toincrease/node-mountebank";

describe("Authorization", () => {
    describe("#Authorize", () => {
        var response: any = {
            "subject": "trn:plantage:company:plantage",
            "resource": "trn:plantage:machine:45678",
            "action": "view",
            "effect": 0
        }

        var imposter = new mb.DefaultImposter(authorizeCommandPath, mb.HttpMethod.POST, response, 200)

        before((done) => {
            imposter.create((err: Error, im: mb.Imposter) => {
                imposter = im;
                done(err)
            });
        })

        let result: authorization.AuthorizationOutput

        let r = new authorization.AuthorizationInput(
            new TRN("plantage", ResourceType.company, "plantage"),
            new TRN("plantage", ResourceType.machine, "45678"),
            "view"
        );

        it("should get status code 200", (done) => {
            let client = new authorization.AuthorizationClient("http://localhost:" + imposter.port)
            let res = client.authorize(newRequestContext(), r)
            res.subscribe(x => result = x, done, done)

        })
        it("action should have the expected value", () => {
            assert.equal(r.action, result.action)
        })
        it("subject should have the expected value", () => {
            assert.deepEqual(r.subject, result.subject)
        })
        it("resource should have the expected value", () => {
            assert.deepEqual(r.resource, result.resource)
        })
        it("the effect should be denied", () => {
            assert.equal(authorization.Effect.deny, result.effect)
        })
    })
})
