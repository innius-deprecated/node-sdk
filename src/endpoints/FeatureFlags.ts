import { SSM } from "aws-sdk";

export class ParameterStore {
    constructor(private client: SSM = new SSM()) { }

    featureEnabled(featureName: string): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            this.client.getParameter({ Name: featureName }, (err, res) => {
                if (err) {
                    return resolve(false)
                }
                resolve(res.Parameter.Value === "true");
            })
        })
    }
}
