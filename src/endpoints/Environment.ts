
import * as http from "http";
import { EC2 } from "aws-sdk"

export class EC2Environment {
    constructor(private client: EC2 = new EC2()) { }

    public stackName(): Promise<string> {
        return getInstanceID()
            .then(x => this.getStackName(x))
            .catch((reason) => {
                console.log(reason)
                return undefined
            })
    }

    private getStackName(instanceID: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.client.describeTags({
                Filters: [
                    { Name: "resource-type", Values: ["instance"] },
                    { Name: "resource-id", Values: [instanceID] },
                    { Name: "key", Values: ["rootstack"] }
                ]
            }, (err, res) => {
                if (err) {
                    return reject(err.message)
                }
                if (res.Tags.length === 0) {
                    return reject("rootstack tag not found")
                }
                resolve(res.Tags[0].Value)
            })
        })
    }
}

function getInstanceID(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        // hack to enable local testing
        if (process.env.LOCALMODE) {
            return resolve("xenon");
        }
        const uri = "http://169.254.169.254/latest/meta-data/instance-id";
        http.get(uri, (res) => {
            const { statusCode } = res;
            if (statusCode !== 200) {
                res.resume();
                return reject(`Request Failed; Status Code: ${statusCode}`);
            }

            res.setEncoding("utf8");
            let rawData = "";

            res.on("data", (chunk) => { rawData += chunk; });
            res.on("end", () => {
                resolve(rawData);
            });
        })
            .on("error", (e) => {
                reject(e.message);
            });
    })
}