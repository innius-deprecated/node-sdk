import { DnsClient } from "./dns";

import { EC2Environment } from "./Environment";
import { ParameterStore } from "./FeatureFlags";

export class ServiceDiscovery {
    constructor(
        private dns: DnsClient = new DnsClient(),
        private environment: EC2Environment = new EC2Environment(),
        private featureFlags: ParameterStore = new ParameterStore()) { }

    async getServiceEndpoint(serviceName: string): Promise<string> {
        let defaultUrl = `http://${serviceName}.service.consul:8080`;

        let stack = await this.environment.stackName()

        let enabled = stack && await this.featureFlags.featureEnabled(`/${stack}/${serviceName}/route53_service_discovery`)

        if (enabled) {
            return this.dns.lookupSRV(`_${serviceName}._tcp.${stack}.servicediscovery.internal`)
                .then(service => `http://${service[0]}:${service[1]}`)
                .catch(_ => defaultUrl)
        }
        return Promise.resolve(defaultUrl);
    }
}

