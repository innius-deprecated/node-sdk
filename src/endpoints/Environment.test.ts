import { EC2Environment } from "./Environment";
import { EC2 } from "aws-sdk";
import { Mock, It } from "typemoq";
import { assert } from "chai"

describe("EC2 Environment", () => {

    it("should return the stack name", async () => {
        let client = Mock.ofType<EC2>()

        client.setup(x => x.describeTags(It.isAny(), It.isAny())).returns((_, callback): any => {
            callback(undefined, { Tags: [{ Value: "foo" }] });
        })

        let env = new EC2Environment(client.object)

        let stackName = await env.stackName();

        assert.equal(stackName, "foo");
    });

})