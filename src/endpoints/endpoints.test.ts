import { assert } from "chai"
import * as endpoints from "./index"

import { ServiceDiscovery } from "./ServiceDiscovery";
import { DnsClient } from "./dns";
import { EC2Environment } from "./Environment";
import { ParameterStore } from "./FeatureFlags";
import { Mock, It } from "typemoq";

describe("Endpoints", function() {
    describe("#normalizeEndpoint", () => {
        describe("endpointWithScheme", async () => {
            var endpoint = "http://localhost:1234";
            var ep = await endpoints.normalizeEndpoint(endpoint, "my-service");
            it("should return the specified endpoint", () => {
                assert.equal(endpoint, ep)
            })
        })
        describe("endpointWithoutScheme", async () => {
            var ep = await endpoints.normalizeEndpoint("localhost:1234", "my-service")
            it("should add the proper scheme", () => {
                assert.equal("http://localhost:1234", ep)
            })
        })
        describe("noEndpointDefined", () => {
            it("should have the default endpoint", async () => {
                let ep = await endpoints.normalizeEndpoint("", "my-service-name");
                assert.equal("http://my-service-name.service.consul:8080", ep)
            })
        })
    })

    describe("Route53-ServiceDiscovery", () => {
        const mock = Mock.ofType<DnsClient>()

        let svc: [string, number] = ["service-hostname", 123];
        mock.setup((x) => x.lookupSRV(It.isAnyString())).returns(() => Promise.resolve(svc));

        let env = Mock.ofType<EC2Environment>()
        env.setup(x => x.stackName()).returns(() => Promise.resolve("xenon"))

        let flags = Mock.ofType<ParameterStore>()
        flags.setup(x => x.featureEnabled(It.isAnyString())).returns(() => Promise.resolve(true))

        let serviceDiscovery = new ServiceDiscovery(mock.object, env.object, flags.object);

        it("should resolve the service endpoint", async () => {
            let ep = await serviceDiscovery.getServiceEndpoint("my-service");
            assert.equal(ep, "http://" + svc.join(":"))
        })

    })

})
