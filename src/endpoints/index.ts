import { ServiceDiscovery } from "./ServiceDiscovery";
import { DnsClient } from "./dns";

const serviceDiscovery = new ServiceDiscovery(new DnsClient(), );

export function normalizeEndpoint(endpoint: string, serviceName: string): Promise<string> {
    if (!endpoint) {
        return serviceDiscovery.getServiceEndpoint(serviceName);
    }
    return Promise.resolve(addScheme(endpoint));
}

const schemeRE = new RegExp("^([^:]+)://");

function addScheme(endpoint: string): string {
    if (endpoint && !schemeRE.test(endpoint)) {
        endpoint = "http://" + endpoint
    }
    return endpoint;
}