import * as dns from "dns";

export class DnsClient {
    lookupSRV(domain: string): Promise<[string, number]> {
        return new Promise((resolve, reject) => {
            dns.resolveSrv(domain, (err, srv: any[]) => {
                if (err) {
                    return reject(err.message)
                }
                resolve(srv.map(x => { return <[string, number]>[<string>x.name, <number>x.port] })[0])
            });
        })
    }
}