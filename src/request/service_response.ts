import * as http from "http"

export interface ServiceReponse {
    raw_body(): any;
    json(): any;
    header(name: string, default_value: any): Header;
    statusCode(): number
    statusMessage(): string
}

export function newServiceResponse(message: http.IncomingMessage, body: any): ServiceReponse {
    return new ServiceResponseImpl(message, body);
}

class ServiceResponseImpl {
    private _headers: any;

    constructor(private message: http.IncomingMessage, private body: any) {
        this._headers = message.headers || {};
    }

    json(): any {
        if (this.body) {
            return JSON.parse(this.body)
        }
    }

    raw_body(): any {
        return this.body;
    }

    header(name: string, default_value: any): Header {
        let val = this._headers[name];
        if (val === undefined) {
            val = default_value;
        }
        return { name: name, value: val }
    }

    statusCode(): number {
        return this.message.statusCode;
    }

    statusMessage(): string {
        return this.message.statusMessage;
    }
}

export interface Header {
    name: string;
    value: string;
}

export function newHeader(name: string, value: string): Header {
    return {
        name: name,
        value: value
    };
}
