import { RequestContext } from "@toincrease/node-context";
import { Observable, Observer } from "@reactivex/rxjs";
import { ServiceReponse, newServiceResponse, Header, newHeader } from "./service_response";

import * as https from "https"
import * as http from "http"
import { Url } from "url"

export interface IRequest {
    send(): Observable<ServiceReponse>
    withPayload(payload: any): IRequest
}

export function newRequest(
    context: RequestContext, method: string, serviceEndpoint: Promise<Url>, ...statusCode: number[]): IRequest {
    return new Request(context, method, serviceEndpoint, ...statusCode)
}

class Request implements IRequest {
    private payload: string
    statusCodes: number[]

    constructor(
        private context: RequestContext,
        private method: string,
        private serviceEndpoint: Promise<Url>,
        ...statusCode: number[]) {

        this.statusCodes = statusCode
    }

    withPayload(payload: any): Request {
        this.payload = JSON.stringify(payload);
        return this
    }

    send(): Observable<ServiceReponse> {
        return Observable.fromPromise(this.serviceEndpoint).flatMap(uri => {
            var options: http.RequestOptions = {
                protocol: uri.protocol,
                port: parseInt(uri.port, 10),
                path: uri.path,
                method: this.method.toUpperCase(),
                headers: {},
                hostname: uri.hostname
            };

            let headers = [
                this.context.getRequestIDHeader(),
                this.context.getLogLevelHeader(),
                newHeader("authorization", `bearer ${this.context.getToken()}`)
            ];

            if (this.payload !== "") {
                headers.push(newHeader("Content-Type", "application/json"));
                headers.push(newHeader("Content-Length", `${Buffer.byteLength(this.payload)}`));
            }

            headers.forEach((header: Header) => {
                options.headers[header.name] = header.value;
            });

            return this.sendRequest(options)
                .publishReplay(1).refCount()
        })
    }

    sendRequest(options: http.RequestOptions): Observable<ServiceReponse> {
        let log = this.context.logger();

        var obs: Observable<ServiceReponse> = Observable.create((o: Observer<ServiceReponse>) => {
            if (o.isUnsubscribed) { return };
            httpClient(options).request(options, (res: http.IncomingMessage) => {
                log.debug("response headers: " + res.headers);
                log.debug("response status code : " + res.statusCode);
                log.debug("response status message: " + res.statusMessage);
                if (this.statusCodes.indexOf(res.statusCode) === -1) {
                    o.error(new Error(
                        `${options.path} @ ${options.hostname}:${options.port} returned an unexpected status: status: ${res.statusCode}; message: ${res.statusMessage}`));
                }
                let data = "";

                res.on("data", (chunk: string) => {
                    if (o.isUnsubscribed) { return };
                    data += chunk;
                })
                res.on("end", () => {
                    if (o.isUnsubscribed) { return };
                    log.debug("response body: " + data);
                    o.next(newServiceResponse(res, data));
                    o.complete();
                })
                res.on("error", (err: Error) => {
                    if (o.isUnsubscribed) { return };
                    o.error(err)
                })
            })
                .end(this.payload)

        });
        return obs
    }
}

function httpClient(options: http.RequestOptions): any {
    if (options.protocol === "https:") {
        return https;
    }
    return http;
}
